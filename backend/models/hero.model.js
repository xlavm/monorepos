const mongoose = require('mongoose')
const Schema = mongoose.Schema

const HeroSchema = new Schema({

    name:{type:String},

}, {
    collection: 'gitlab-monorepos-collection'
});

module.exports = mongoose.model('hero', HeroSchema);
