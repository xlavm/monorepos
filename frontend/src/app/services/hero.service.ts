
import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { environment } from '../../environments/environment'
import Hero  from '../models/hero.model'

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  uri = environment.URI_BACK

  constructor(private http: HttpClient) { }

  addHero(hero: Hero) {
    return this.http.post(`${this.uri}/`, hero)
  }

  getHeroes() {
    return this.http.get(`${this.uri}`)
  }

  getHero(id:any) {
    return this.http.get(`${this.uri}/${id}`)
  }

  updateHero(hero: Hero) {
    return this.http.put(`${this.uri}/${hero._id}`, hero)
  }

  deleteHero(id:any) {
    return this.http.delete(`${this.uri}/${id}`)
  }

  save(hero: Hero) {
    if (hero._id) {
      return this.http.put(`${this.uri}/${hero._id}`, hero)
    }
    return this.http.post(`${this.uri}/`, hero)
  }

}