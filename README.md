# Monorepos

this project contain the monorepos implement

## Repositories Types

![monorepos](images/monorepos1.png)
Tomado de: https://www.toptal.com/front-end/guide-to-monorepos

## Monorepo Flow

![monorepo-flow](images/monorepo-flow.png)

## Monorepo Structure

```
monorepo/
    backend/
        src/
        Dockerfile
    frontend/
        src/
        Dockerfile
    .git/
    .gitignore
    .gitlab-ci.yaml
    docker-compose.yaml
```

## Monorepo for Angular

Change the `package.json` command for start and build with:

```JSON
  {
    "start:front": "copy .\\frontend\\angular.json .\\ && ng serve",
    "build:front": "copy .\\frontend\\angular.json .\\ && ng build"
  }
```

Change the `angular.json` file with the roots: `src/` to the next roots: `frontend/src`, ALERT!!! make sure to change the: `"tsConfig": "frontend/tsconfig.app.json",`

so that the build file of angular have name equal to dist-front, I do changed in the angular.json file the name of outputPath propertie:

```JSON
  {
    "outputPath": "dist-front",
  }
```

## Monorepo CI/CD

In the .gitlab-ci.yaml we define jobs for each of our services and for each stage. To ensure that a job for a service runs only if the source code of this service has been changed, we can use the only / changes clause in combination with a regular expression of the folder path. For example, the backend service build job could be defined as follows:

```
backend_build:
  stage: build
  only:
    changes:
      - "backend/**/*"
  ...
```

Create the `.env` file in the equal route of package.json with this:

```
PORT=3000
PATH_MONGO=mongodb+srv://manager-apis-back-user:dyldMQYT7HV*&HwEtRc7W!H@manager-apis-back-clust.z1hax.mongodb.net/manager-apis-back-db?retryWrites=true&w=majority
PATH_API_HERO=/api/v1/hero
```

## Executing local 

create the `.env` in the package.json route and paste the content of CI/CD Variable: ENV_VARIABLES

![ci-cd-variables](images/ci-cd-variables.png)